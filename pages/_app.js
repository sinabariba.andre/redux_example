import '../styles/globals.css'
import App from 'next/app'
import {Provider} from 'react-redux'
import store from '../store/store'

class MyApp extends App{
  render(){
    const { Component, pageProps } =this.props
    return (
    <Provider store={store}>
      <Component {...pageProps} />
    </Provider>
    )
  }
}

export default MyApp
