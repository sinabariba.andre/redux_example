import {combineReducers} from 'redux'
import counterReducer from './counter.reducer'

const rootReducer = combineReducers({
    // import smua reducer disini
    counter: counterReducer

})

export default rootReducer