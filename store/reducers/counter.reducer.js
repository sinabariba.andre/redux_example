// import smua action dr counter.action
import {TAMBAH,KURANG, TAMBAH_KUOTA, KURANG_KUOTA,UBAH_KUOTA} from '../actions/counter.action'

const initialState = {
    counterval:0,
    kuotaval:0
}

export const counterReducer= (state = initialState, action )=> {
    switch (action.type) {
    case TAMBAH:
        return {...state, counterval : state.counterval + 1 }

    case KURANG:
        return {...state, counterval : state.counterval  - 1 }

    case TAMBAH_KUOTA:
        return {...state, counterval: state.counterval + action.payload}

    case KURANG_KUOTA:
        return {...state, counterval: state.counterval - action.payload}

    case UBAH_KUOTA:
        return {...state,kuotaval: action.payload}

    default:
        return state
    }
}

export default counterReducer
