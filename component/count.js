// membuat tampilan counter
import React from 'react'
import {connect } from 'react-redux'

// menggabungkan state dr reducer ke class 
const mapStateToProps=state=>({
    // <variable>:state.<nama_export_state_dr_rootReducer>
    ctr:state.counter
})

class Count extends React.Component {
   
    // function handleChange input
    changeHandler(event){
        // (event) data yg diterima
        //  kuota akan update automatis 
        this.props.dispatch({
            type:'UBAH_KUOTA',
            payload:event.target.value
        })
    }
  render(){
    //   destructuring dari const mapStateToProps
      const {ctr}= this.props
        return (
            <div>
                <h2>Current Counter {ctr.counterval} </h2>
                <div>
                    <button  onClick={()=>this.props.dispatch({type:'TAMBAH'})}>Tambah</button>
                    <button onClick={()=>this.props.dispatch({type:'KURANG'})}>Kurang</button>
                    <button  onClick={()=>this.props.dispatch({type:'TAMBAH_KUOTA',payload:parseInt(ctr.kuotaval)})}>Tambah Kuota{ctr.kuotaval}</button>
                    <button onClick={()=>this.props.dispatch({type:'KURANG_KUOTA', payload:parseInt(ctr.kuotaval)})}>Kurang Kuota {ctr.kuotaval}</button>
                </div>
                    <input type="number" onChange={(event)=>this.changeHandler(event)}/>
            </div>
        )
    }
}

export default connect(mapStateToProps,null)(Count)
